FROM ubuntu:22.04
MAINTAINER rleigh@codelibre.net

RUN apt-get clean && apt-get update && apt-get dist-upgrade -y -qq --no-install-recommends \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --no-install-recommends \
    build-essential \
    ca-certificates \
    curl \
    doxygen \
    git \
    jq \
    man \
    locales \
    graphviz \
    zip \
    unzip \
    libgtest-dev \
    libfmt-dev \
    ninja-build \
  && locale-gen en_US.UTF-8 \
  && locale-gen pt_BR.UTF-8

RUN curl -Lo cmake.tar.gz https://github.com/Kitware/CMake/releases/download/v3.26.4/cmake-3.26.4-linux-x86_64.tar.gz \
    && echo "ba1e0dcc710e2f92be6263f9617510b3660fa9dc409ad2fb8190299563f952a0 *cmake.tar.gz" | sha256sum --check - \
    && cd /usr/local \
    && tar --strip-components=1 -xvf /cmake.tar.gz \
    && rm /cmake.tar.gz


ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
